/*
 *  ======== main_nortos.c ========
 */
#include <stdint.h>
#include <stddef.h>

#include <NoRTOS.h>

#include <ti/drivers/Board.h>
#include <ti/drivers/gpio/GPIOCC26XX.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/apps/LED.h>




/* Driver configuration */
#include "ti_drivers_config.h"

#define QUUPPA_ANT_NUM 13
#define CORELINK_ANT_NUM 16

#define QUUPPA_TOGGLING_USRP
#define QUUPPA_TOGGLING_VNA
#define CORELINK_TOGGLING_USRP    //采USRP数据
//#define CORELINK_TOGGLING_VNA     //采网分数据

#define PACKET_LEN      4           // 封窗数据长度
#define START_PACKET    0x7E        // 封装数据头
#define END_PACKET      0xE7        // 封装数据尾

#define START_POSITION      0
#define END_POSITION        3
#define DATA_POSITION_0     1       // 第一个数据位
#define DATA_POSITION_1     2       // 第二个数据位

#define ANT_NUM             16
#define CTL_NUM             4

// 天线开关真值表
// ANT[n][m]表示第n根天线的 ctl_m 控制线电平

// QUPPA天线板     0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
//    {ctrl0, ctrl1, ctrl2, ctrl3}
//                          {1, 1, 0, 0}, {1, 1, 0, 1}, {1, 1, 1, 0}, {1, 1, 1, 1},
//                          {0, 0, 0, 0}, {0, 0, 0, 1}, {0, 0, 1, 0}, {0, 0, 1, 1},
//                          {1, 0, 0, 0}, {1, 0, 0, 1}, {1, 0, 1, 0}, {1, 0, 1, 1},
//                          {0, 1, 0, 0}

//  Corelink天线板     0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
//             {ctrl3, ctrl2, ctrl1, ctrl0}             {0, 1, 0, 1}, {1, 0, 0, 1}, {1, 1, 0, 1}, {0, 0, 1, 1},
//                                                      {1, 1, 1, 0}, {0, 1, 1, 0}, {0, 1, 1, 1}, {1, 0, 1, 1},
//                                                      {1, 1, 1, 1}, {1, 0, 0, 0}, {0, 0, 0, 0}, {1, 1, 0, 0},
//                                                      {1, 0, 1, 0}, {0, 1, 0, 0}, {0, 0, 0, 1}, {0, 0, 1, 0}

// QUPPA天线板切换序列，寄存器
#ifndef QUUPPA_TOGGLING
const int8_t ANT_TABLE_REG[QUUPPA_ANT_NUM] = {
                          0x03, 0x0b, 0x07, 0x0f,   // 0x03对应的是{0, 0, 1, 1}分别对应DIO30、DIO29、DIO28、DIO27。QUPPA天线板中{1, 1, 0, 0}则对应的是DIO27、DIO28、DIO29、DIO30
                          0x00, 0x08, 0x04, 0x0c,
                          0x01, 0x09, 0x05, 0x0d,
                          0x02
};
//const int8_t ANT_TABLE_REG[QUUPPA_ANT_NUM] = {
//                          0x00, 0x08, 0x04, 0x0c,
//                          0x02, 0x0a, 0x6,
//};
#endif

#ifndef CORELINK_TOGGLING_USRP
const uint32_t ANT_TABLE_REG[CORELINK_ANT_NUM]= {
                          0x09, 0x05, 0x03, 0x0d,
                          0x06, 0x0e, 0x0b, 0x07,
                          0x0f, 0x00, 0x08, 0x04,
                          0x02, 0x0c, 0x01, 0x0a
};
#endif

#ifndef QUUPPA_TOGGLING
const int8_t PN_Sequence[] = {12,12,9,9,3,2,6,11,0,
                              12,12,12,8,2,12,2,12,12,2,7,10,12,11,
                              12,12,12,1,10,
                              12,12,12,5,1,12,12,7,
                              12,12,12,11,12,11,12,12,2,4,1,12,6,9,5,
                              12,12,12,12,10,3,12,8,
                              12,12,12,12,10,12,12,0,2,11,12,12,0,12,5,12,8,
                              12,12,12,0,
                              12,12,12,12,9,12,
                              12,12,12,3,12,12,7,2,12,12,1,12,12,1,12,12,3,3,12,5,
                              12,12,12,12,12,8,5,6,11,5,
                              12,12,12,10,7,12,6,12,12,5,12,7,3,12,3,2,5,3,1,
                              12,11,6,12,3,12,7,4,12,12,5,
                              12,11,12,12,12,12,12,5,12,12,
                              12,12,8,12,11,8,4,3,12,12,11,
                              12,12,11,8,7,11,11,8,5,12,2,
                              12,12,2,12,3,12,12,12,1,12,1,
                              12,12,12
};
//const int8_t PN_Sequence[] = {1,2,1,3};

//const int8_t pattern[] = {12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 0, 1, 2, 0, 1, 3, 0, 1, 4, 0, 1, 5, 0, 1, 6, 0, 1, 7, 0, 1, 8, 0, 1, 9, 0, 1, 10, 0, 1, 11};
const int8_t pattern[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
#else
const int8_t pattern[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
//const int8_t pattern[] = {11};
#endif

#ifdef CORELINK_TOGGLING_VNA
// patch1~patch16
const uint8_t ANT_TABLE[ANT_NUM][CTL_NUM]= {
                          {1, 0, 0, 1}, {1, 0, 1, 0}, {1, 1, 0, 0}, {1, 0, 1, 1},
                          {0, 1, 1, 0}, {0, 1, 1, 1}, {1, 1, 0, 1}, {1, 1, 1, 0},
                          {1, 1, 1, 1}, {0, 0, 0, 0}, {0, 0, 0, 1}, {0, 0, 1, 0},
                          {0, 1, 0, 0}, {0, 0, 1, 1}, {1, 0, 0, 0}, {0, 1, 0, 1}
};

const uint32_t ANT_TABLE_REG[CORELINK_ANT_NUM]= {
                          0x09, 0x05, 0x03, 0x0d,
                          0x06, 0x0e, 0x0b, 0x07,
                          0x0f, 0x00, 0x08, 0x04,
                          0x02, 0x0c, 0x01, 0x0a
};
#endif

#ifndef QUUPPA_TOGGLING
uint32_t PN_Sequence_len = sizeof(PN_Sequence)/sizeof(PN_Sequence[0]);
#endif
int8_t pattern_len = sizeof(pattern)/sizeof(pattern[0]);

LED_Handle ledHandle;

void KeepChangingAntWithPN(const int8_t *PN_SEQ, uint32_t PN_SEQ_len, const int8_t *ANT_TABLE, int8_t ANT_TABLE_len){
    int8_t antId;
    uint32_t count = 0;

    for (antId = 0; antId < PN_SEQ_len; ++antId ){
        GPIO_writeMultiDio(GPIO_DIO_27_MASK | GPIO_DIO_28_MASK | GPIO_DIO_29_MASK | GPIO_DIO_30_MASK, ANT_TABLE_REG[PN_SEQ[antId]] << 27);
        for(count = 0; count < 8; count++){
            ;
        }
    }

    for (antId = 0; antId < ANT_TABLE_len; ++antId ){
        GPIO_writeMultiDio(GPIO_DIO_27_MASK | GPIO_DIO_28_MASK | GPIO_DIO_29_MASK | GPIO_DIO_30_MASK, ANT_TABLE_REG[ANT_TABLE[antId]] << 27);
        //delay 4.22us
        for(count = 0; count < 22; count++){
            ;
        }

    }
}

void KeepChangingAntWithoutPN(const int8_t *ANT_TABLE, int8_t ANT_TABLE_len){
    int8_t antId;
    uint32_t count = 0;

    for (antId = 0; antId < ANT_TABLE_len; ++antId ){
        GPIO_writeMultiDio(GPIO_DIO_27_MASK | GPIO_DIO_28_MASK | GPIO_DIO_29_MASK | GPIO_DIO_30_MASK, ANT_TABLE_REG[ANT_TABLE[antId]] << 27);
        //delay 4.22us
        for(count = 0; count < 22; count++){
            ;
        }

    }
}

#ifdef CORELINK_TOGGLING_VNA
// 进行天线切换控制（控制线逐位写）
void changeAnt(UART_Handle uart ,char *buf){
    if(buf[START_POSITION] == START_PACKET && buf[END_POSITION] == END_PACKET){
        if(buf[DATA_POSITION_0] == buf[DATA_POSITION_1]){
            char antId = buf[DATA_POSITION_0];
            if(antId < ANT_NUM){
                GPIO_write(CTL0, ANT_TABLE[antId][0]);
                GPIO_write(CTL1, ANT_TABLE[antId][1]);
                GPIO_write(CTL2, ANT_TABLE[antId][2]);
                GPIO_write(CTL3, ANT_TABLE[antId][3]);
                LED_write(ledHandle, LED_ON);
                usleep(50000);
                LED_write(ledHandle, LED_OFF);
                UART_write(uart, buf, PACKET_LEN);
            }
        }
    }
}

// 进行天线切换控制（写寄存器）
void changeAntReg(UART_Handle uart ,char *buf){
    if(buf[START_POSITION] == START_PACKET && buf[END_POSITION] == END_PACKET){
        if(buf[DATA_POSITION_0] == buf[DATA_POSITION_1]){
            char antId = buf[DATA_POSITION_0];
            if(antId < ANT_NUM){
                GPIO_writeMultiDio(GPIO_DIO_27_MASK | GPIO_DIO_28_MASK | GPIO_DIO_29_MASK | GPIO_DIO_30_MASK, ANT_TABLE_REG[antId] << 27);
                LED_write(ledHandle, LED_ON);
                usleep(50000);
                LED_write(ledHandle, LED_OFF);
                UART_write(uart, buf, PACKET_LEN);
            }
        }
    }
}
#endif

#ifdef QUUPPA_TOGGLING_VNA
// 进行天线切换控制（写寄存器）
void changeAntReg(UART_Handle uart ,char *buf){
    if(buf[START_POSITION] == START_PACKET && buf[END_POSITION] == END_PACKET){
        if(buf[DATA_POSITION_0] == buf[DATA_POSITION_1]){
            char antId = buf[DATA_POSITION_0];
            if(antId < ANT_NUM){
                GPIO_writeMultiDio(GPIO_DIO_27_MASK | GPIO_DIO_28_MASK | GPIO_DIO_29_MASK | GPIO_DIO_30_MASK, ANT_TABLE_REG[antId] << 27);
                LED_write(ledHandle, LED_ON);
                usleep(50000);
                LED_write(ledHandle, LED_OFF);
                UART_write(uart, buf, PACKET_LEN);
            }
        }
    }
}
#endif


extern void *mainThread(void *arg0);

/*
 *  ======== main ========
 */
int main(void)
{
    Board_init();

    /* Start NoRTOS */
    NoRTOS_start();

    char        buf[PACKET_LEN];

    UART_Handle uart;
    UART_Params uartParams;
    LED_Params  ledParams;

    /* Call driver init functions */
    GPIO_init();
    LED_init();
    UART_init();

    LED_Params_init(&ledParams);
    ledHandle = LED_open(CONFIG_LED_0, &ledParams);
    //    LED_startBlinking(ledHandle, 1000, LED_BLINK_FOREVER);
    LED_write(ledHandle, LED_OFF);

    /* Create a UART with data processing off. */
    UART_Params_init(&uartParams);
    uartParams.writeDataMode = UART_DATA_BINARY;
    uartParams.readDataMode = UART_DATA_BINARY;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.baudRate = 115200;

    uart = UART_open(CONFIG_UART_0, &uartParams);

    if (uart == NULL) {
        /* UART_open() failed */
        while (1);
    }

    /* Call mainThread function */
    //mainThread(NULL);
    long i = 0;
    while (1) {
//        KeepChangingAntWithoutPN(PN_Sequence, PN_Sequence_len, pattern, pattern_len);
#ifndef QUUPPA_TOGGLING_USRP
        KeepChangingAntWithoutPN(pattern, pattern_len);     //以pattern中的切换顺序不带PN码进行切换
#endif

#ifndef CORELINK_TOGGLING_USRP
        KeepChangingAntWithoutPN(pattern, pattern_len);
#endif

#ifdef CORELINK_TOGGLING_VNA
        UART_read(uart, buf, PACKET_LEN);
        changeAntReg(uart, buf);
        // delay 8.4us
//        for(i = 0; i < 50; ++i){
//            ;
//        }
#endif

#ifdef QUUPPA_TOGGLING_VNA
        UART_read(uart, buf, PACKET_LEN);
        changeAntReg(uart, buf);
        // delay 8.4us
//        for(i = 0; i < 50; ++i){
//            ;
//        }
#endif
    }
}


